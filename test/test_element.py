from unittest import TestCase
import element as _el

class TestLinElement(TestCase):

    def test_creation(self):
        coords = (0, 1)
        self.assertEqual(_el.LinElement(coords).get_coords(), coords)


class TestElementFactory(TestCase):

    def test_linear_element(self):
        expected_coords = [(0, 0.25), (0.25, 0.5), (0.5, 0.75), (0.75, 1)]
        _, elements = _el.ElementFactory.get_elements_for_domain(omega=(0, 1), n_els=4, el_type=_el.ElementType.LIN)
        self.assertEqual([el.get_coords() for el in elements], expected_coords)

    def test_quadratic_element(self):
        expected_coords = [(0, 0.1, 0.2), (0.2, 0.3, 0.4)]
        _, elements = _el.ElementFactory.get_elements_for_domain(omega=(0, 0.4), n_els=2, el_type=_el.ElementType.QUAD)
        for i, el in enumerate(elements):
            for j, coord in enumerate(el.get_coords()):
                self.assertAlmostEqual(coord, expected_coords[i][j], delta=0.005)


    def test_linear_interpolant(self):
        coords = (0.2, 0.4)
        el = _el.LinElement(coords)
        interpolants = el.get_interpolators()

        test_vector = [
            [(0.2, 1), (0.3, 0.5), (0.4, 0)],
            [(0.2, 0), (0.3, 0.5), (0.4, 1)]
        ]
        for idx, inter in enumerate(interpolants):
            test = test_vector[idx]
            for case in test:
                arg, expected_value = case
                self.assertAlmostEqual(expected_value, inter(arg), msg=f"{idx}, {arg}", delta=0.0001)

    def test_quadratic_interpolant(self):
        coords = (-0.5, 0, 0.5)
        el = _el.QuadElement(coords)
        interpolants = el.get_interpolators()

        test_vector = [
            [[-0.5, 1], [0, 0], [0.25, -0.125], [0.5, 0]],
            [[-0.5, 0], [-0.4, 0.36], [0, 1], [0.3, 0.64], [0.5, 0]],
            [[-0.5, 0], [-0.25, -0.125], [0, 0], [0.5, 1]]
        ]
        for idx, inter in enumerate(interpolants):
            test = test_vector[idx]
            for case in test:
                arg, expected_value = case
                self.assertAlmostEqual(expected_value, inter(arg), msg=f"{idx}, {arg}", delta=0.0001)