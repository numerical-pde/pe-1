import numpy as np
from unittest import TestCase
from util import ElementType
from basis import *


class TestHatBasis(TestCase):

    def test_basis_creation(self):
        disc_domain = np.linspace(0, 1, 5)
        basis = BasisFactory.get_basis_for_domain(disc_omega=disc_domain, el_type=ElementType.LIN)
        self.assertEqual(len(basis), 3)

        for i, x in enumerate(disc_domain[1: -1]):
            # basis needs to sail at interior nodes
            _, _x, _ = basis[i].get_coords()
            self.assertEqual(x, _x)


class TestQuadraticBasis(TestCase):

    def test_basis_creation(self):
        disc_domain = np.linspace(0, 1, 5)
        basis = BasisFactory.get_basis_for_domain(disc_omega=disc_domain, el_type=ElementType.QUAD)
        self.assertEqual(len(basis), 3)
        expected_coords = [tuple(disc_domain[:3]), tuple(disc_domain), tuple(disc_domain[2:])]

        for i, b in enumerate(basis):
            self.assertEqual(expected_coords[i], b.get_coords())

    def test_both_odd_basis_der(self):
        bi = Quadratic((-0.5, 0, 0.5))
        bj = Quadratic((-0.5, 0, 0.5))
        aij = bi | bj
        aji = bj | bi
        self.assertEqual(aij, aji)

    def test_both_even_basis_der(self):
        bi = Quadratic((-0.5, -0.25, 0, 0.25, 0.5))
        bj = Quadratic((-0.5, -0.25, 0, 0.25, 0.5))
        aij = bi | bj
        aji = bj | bi
        self.assertEqual(aij, aji)

        bi = Quadratic((-0.5, -0.25, 0, 0.25, 0.5))
        bj = Quadratic((-1, -0.75, -0.5, -0.25, 0))
        aij = bi | bj
        aji = bj | bi
        self.assertEqual(aij, aji)

    def test_odd_even_basis_der(self):
        bi = Quadratic((-0.5, -0.25, 0, 0.25, 0.5))
        bj = Quadratic((0, 0.25, 0.5))
        aij = bi | bj
        aji = bj | bi
        self.assertEqual(aij, aji)

        bi = Quadratic((-0.5, -0.25, 0, 0.25, 0.5))
        bj = Quadratic((0.75, 1, 1.25))
        aij = bi | bj
        aji = bj | bi
        self.assertEqual(aij, aji)
        self.assertEqual(aij, 0)

