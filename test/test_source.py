from unittest import TestCase
from basis import *
from source import *

class TestSource(TestCase):

    def test_combination_of_source_and_quadratic_basis(self):
        source = ConstantSource(1)
        bi = Quadratic((0.1, 0.25, 0.26))
        bj = Quadratic((0.45, 0.6, 0.61))
        self.assertAlmostEqual(source | bi, source | bj, delta=0.0001)

        bi = Quadratic((0, 0.1, 0.25, 0.26, 0.5))
        bj = Quadratic((0.35, 0.45, 0.6, 0.61, 0.85))
        self.assertAlmostEqual(source | bi, source | bj, delta=0.0001)
