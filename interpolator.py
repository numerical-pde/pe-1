from typing import List
from element import Element

class Interpolator:
    def __init__(self, elements: List[Element]):
        self.elements = elements

    def intepolate(self, x: float) -> float:
        interpolated_result = 0
        f_miss = True
        for _el in self.elements:
            if _el.get_within_range(x):
                for intepolator in _el.get_interpolators():
                    interpolated_result += intepolator(x)
                f_miss = False
                break
        if(f_miss): raise Exception('Value outside of computation domain')
        return interpolated_result