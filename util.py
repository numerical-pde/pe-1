from enum import Enum
from typing import Callable, Tuple
from scipy.integrate import quad


class ElementType(Enum):
    LIN = 1
    QUAD = 2


class ProblemType(Enum):
    DIRAQ = 1
    CONST = 2


class L2ErrorComputer():
    def __init__(self, exact_sol: Callable[[float], float], approx_sol: Callable[[float], float]):
        self.exact_sol = exact_sol
        self.approx_sol = approx_sol

    def integrand(self):
        return lambda x: (self.exact_sol(x) - self.approx_sol(x))**2

    def eval(self, domain: Tuple[float, float]):
        a, b = domain
        return quad(self.integrand(), a, b)[0]