from abc import abstractmethod, ABC
from typing import Tuple, Callable
import numpy as np
from basis import *
from util import ElementType


class Element(ABC):
    @abstractmethod
    def get_coords(self) -> Tuple[float]:
        pass

    @abstractmethod
    def get_basis(self) -> Tuple[Basis]:
        pass

    @abstractmethod
    def get_within_range(self, x:float) -> bool:
        pass

    """
    Returns a set of interpolators to calculate
    the value of an arbitrary x within the domain.
    u(x) - sum of scaled by discrete slution interpolators evalutaed at x.

    Note:
    To work properly it needs to be applied on an element domain only!
    """
    @abstractmethod
    def get_interpolators(self) -> Tuple[Callable[[float], float]]:
        pass

    @abstractmethod
    def set_solutions(self, solutions: Tuple[float]):
        pass


class LinElement(Element):
    _type: ElementType = ElementType.LIN
        
    def __init__(self, coords: Tuple[float, float], basis: Tuple[Hat, Hat] = None):
        self.coords = coords
        self.basis = basis
        self.solutions = (1, 1)

    def get_basis(self):
        return self.basis
    
    def get_coords(self):
        return self.coords

    def get_within_range(self, x: float) -> bool:
        x_m, x_p = self.coords
        return x >= x_m and x <= x_p

    def get_interpolators(self):
        x_m, x_p = self.coords
        return (
            lambda x: self.solutions[0] * (x - x_p) / (x_m - x_p),
            lambda x: self.solutions[1] * (1 + (x_p - x) / (x_m - x_p))
        )
    
    def set_solutions(self, solutions: Tuple[float, float]):
        self.solutions = solutions

    def __str__(self):
        return f"{self._type}: {self.coords} {self.solutions}"


class QuadElement(Element):
    _type: ElementType = ElementType.QUAD
        
    def __init__(self, coords: Tuple[float, float, float]):
        self.coords = coords
        self.solutions = (1, 1, 1)
    
    def get_coords(self):
        return self.coords

    def get_basis(self):
        pass

    def get_within_range(self, x: float) -> bool:
        x_m, _, x_p = self.coords
        return x >= x_m and x <= x_p

    def get_interpolators(self):
        x_m, _x, x_p = self.coords
        return (
            lambda x: self.solutions[0] * (x - _x) * (x - x_p) / (x_m - _x) / (x_m - x_p),
            lambda x: self.solutions[1] * (x - x_m) * (x - x_p) / (_x - x_m) / (_x - x_p),
            lambda x: self.solutions[2] * (x - _x) * (x - x_m) / (x_p - _x) / (x_p - x_m)
        )

    def set_solutions(self, solutions: Tuple[float, float, float]):
        self.solutions = solutions
    
    def __str__(self):
        return f"{self._type}: {self.coords} {self.solutions}"


class ElementFactory:
    """
    Params:
    --------
    omega: (float, float)
        domain boundary points, e.g. (0, 1)
    n_els: int
        number of elements
    el_type: ElementType
    
    Return:
    --------
    numpy.ndarray: size = n + 2, discretized domain with boundary points included
    List[Element]: list of linear elements for the domain
    """
    def get_elements_for_domain(omega: Tuple[float, float], n_els: int, el_type: ElementType = ElementType.LIN):
        a, b = omega
        if el_type == ElementType.LIN:
            # 1 el == 1 interval -> n = n_els - 1
            n = n_els - 1
            step = (b - a) / (n + 1)
            els = [
                LinElement((
                    a + step * (i),
                    a + step * (i + 1)
                )) for i in range(n_els)]
        elif el_type == ElementType.QUAD:
            # 1 el == 2 intervals -> n = 2*n_els - 1, AWESOME always odd
            n = 2 * n_els - 1
            step = (b - a) / (n + 1)
            els = [
                QuadElement((
                    a + step * (2 * i),
                    a + step * (2 * i + 1),
                    a + step * (2 * i + 2)
                )) for i in range(n_els)
            ]
        disc_domain = np.linspace(a, b, n + 2)
        return disc_domain, els