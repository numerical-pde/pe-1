# FEM. PE1.

- Understand the problem setting
	- Variational formulation in the discrete/linearized form.
		How does the transition from the continuous operators to the discrete system of linear equations occure?
		1. First thing to recognize is how terms disappear from the more complete formulatin of the Poisson equation based on the boundary conditions chosen, as well as the function space fixed.
		Test functions, which are used to extend the notion of *derivative* to *weak derivative* are restricted to the H^{1}_{0} space, which means that both u and v at the boundary dissappear causing the line integral to disappear.
		
		Bilinear mapping arises due to linearity of the integration operator, therefore there is a mathematical equivalence between the bilinear mapping a and the process of integratin that appears in the variational formulation of the PDE.
		- a(u, v) = l(v), is therefore a variational formulation written after formalization of the notation

- Main strategy behind FEM
	- What are elements? 
	- What is the finite basis?
	- How does finite basis help approximate the solution?
	- What would a linear combination of the hat functions result in on the interval?

At the moment my code performs computation with using interior nodes at its core, and as a
nucleous. Invert the perspective:
- [x] Basis is defined in terms of nodes
- [ ] Create a strcuture of ordered elements
- [ ] Map an element to a list of basis which corresponds to the interval

- [ ] Each element is a piece of domain on which the approximation is defined in terms function.
	Elements have certain properties, which are specifically defined to demand certain behaviour:
		- At each node a function needs to have an exact solution -> u(p_{i}) = 1
		this ensures that after inverting the matrix, alfa will correspond to an exact solution.
		- We demand for every node within the element to give full contribution from its presence.
			- linear - (1, 0), (0, 1)
			- quadratic - (1, 0, 0), (0, 1, 0), (0, 0, 1)
			...

Important question to keep in mind, is why should we benefit interior points over all other points.
This mindset is to specific, and is in principle convenient only in 1D Dirichlet. It seems that in
2D it already becomes inconvenient to differentiate the points. However, it is worth noting, that
from the perspective of basis definition in case of Dirichlet the solutions are demanded to be in
H_{0} so there is an encoded difference between the points in terms of basis existence.