# when fm2cplib is imported, provide a routine to load the stylesheet and macros for the exercise notebooks

from IPython.core.display import HTML

def load_styles_and_macros():
    """ Load the stylesheet and TeX macros for the homeworks! """
    macros = open("lib/macros.tex", "r").read()
    style = open("lib/exercises_style.css", "r").read()
    styles = "<style>{0}</style>\n $${1} \n$$".format(style, macros)
    return HTML(styles)

