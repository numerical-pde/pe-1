from abc import ABC, abstractmethod
from typing import Tuple, List, Union
import numpy as np
from util import ElementType


class Basis(ABC):
    @abstractmethod
    def get_coords(self):
        pass
    
    @abstractmethod
    def get_val(self, x: float):
        pass
    
    @abstractmethod
    def get_der(self, x: float):
        pass
    
    """
    Defines an operator which serves as a substitute equivalent to a(bi, bj):
    bi = Basis(...)
    bj = Basis(...)
    bi | bj := a(bi, bj)
    """
    @abstractmethod
    def __or__(self, bj):
        pass

    def __str__(self):
        return f"{self.__class__.__name__}: {self.get_coords()}"


class Hat(Basis):
    def __init__(self, coords: Tuple[float, float, float]):
        self.coords = coords

    def get_coords(self):
        return self.coords
    
    def get_val(self, x: float):
        _x_m, _x, _x_p = self.coords
        if x < _x_m or x > _x_p:
            return 0
        elif x >= _x_m and x <= _x:
            return (x - _x_m) / (_x - _x_m)
        else:
            return (_x_p - x) / (_x_p - _x)
    
    def get_der(self, x):
        _x_m, _x, _x_p = self.coords
        if x < _x_m or x > _x_p:
            return 0
        elif x >= _x_m and x <= _x:
            return 1 / (_x - _x_m)
        else:
            return -1 / (_x_p - _x)
    
    def __or__(self, bj):
        """
        Apply the mathematics trick, because we know the values before hand
        from simple relationship between the pair of basis functions =)
        """
        assert isinstance(bj, Hat), "Only combination of two Hat functions is supported at the moment."
        
        _, x_i, _ = self.get_coords()
        x_j_m, x_j, x_j_p = bj.get_coords()
        if x_i == x_j:
            return 1 / (x_i - x_j_m) + 1 / (x_j_p - x_i)
        elif x_i == x_j_m or x_i == x_j_p:
            return -1 / abs(x_i - x_j)
        else: return 0


class Quadratic(Basis):
    def __init__(
        self,
        coords: Union[Tuple[float, float, float], Tuple[float, float, float, float, float]]
    ):
        self.f_odd = len(coords) == 3
        self.coords = coords
        self._set_spread_coefficient()

    # TODO: substitute where relevant
    def _set_spread_coefficient(self):
        if self.f_odd:
            x_m, _x, x_p = self.coords
            self.A = 1 / (_x - x_m) / (_x - x_p)
        else:
            x_mm, x_m, _x, x_p, x_pp = self.coords
            self.A = 1 / (_x - x_mm) / (_x - x_m), 1 / (_x - x_pp) / (_x - x_p)

    def get_val(self, x):
        if self.f_odd:
            x_m, _x, x_p = self.coords
            if x < x_m or x > x_p:
                return 0
            else:
                return self.A * (x - x_m) * (x - x_p)
        else:
            x_mm, x_m, _x, x_p, x_pp = self.coords
            if x < x_mm or x > x_pp:
                return 0
            elif x >= x_mm and x <= _x:
               return self.A[0] * (x - x_mm) * (x - x_m)
            else:
                return self.A[1] * (x - x_pp) * (x - x_p)

    def get_coords(self):
        return self.coords

    def get_f_odd(self):
        return self.f_odd

    def get_der(self, x: float):
        if self.f_odd:
           x_m, _x, x_p = self.coords
           if x < x_m or x > x_p:
               return 0
           else:
               return self.A * ((x - x_p) + (x - x_m))
        else:
            x_mm, x_m, _x, x_p, x_pp = self.coords
            if x < x_mm or x > x_pp:
                return 0
            elif x >= x_mm and x <= _x:
               return self.A[0] * ((x - x_mm) + (x - x_m))
            else:
                return self.A[1] * ((x - x_pp) + (x - x_p))

    def __or__(self, bj):
        assert isinstance(bj, Quadratic)
        diff = lambda x1, x2, n: x1**n - x2**n
        f_same = self.f_odd == bj.get_f_odd()
        if f_same:
            if self.f_odd:
                # only overlap is possible
                if self.coords[1] == bj.get_coords()[1]:
                    x_m, _, x_p = self.coords
                    sum = x_p + x_m
                    return  self.A**2 * (4 * diff(x_p, x_m, 3) / 3 - 2 * diff(x_p, x_m, 2) * sum + sum**2 * diff(x_p, x_m, 1))
                else: return 0
            else:
                if self.coords[2] == bj.get_coords()[2]:
                    # we have an overlap
                    x_mm, x_m, _x , x_p, x_pp= self.coords
                    sum_l, sum_r = x_mm + x_m, x_p + x_pp
                    return self.A[0]**2 * (4 / 3 * diff(_x, x_mm, 3) - 2 * diff(_x, x_mm, 2) * sum_l + sum_l**2 * diff(_x, x_mm, 1)) + self.A[1]**2 * (4 / 3 * diff(x_pp, _x, 3) - 2 * diff(x_pp, _x, 2) * sum_r + sum_r**2 * diff(x_pp, _x, 1))
                else:
                    left, right = (bj, self) if self.coords[2] > bj.get_coords()[2] else (self, bj)
                    _, _, _x_l, x_l_p, x_l_pp = left.get_coords()
                    x_r_mm, x_r_m, _x_r, _, _ = right.get_coords()
                    if x_l_p == x_r_m:
                        A_l, A_r = left.A[1], right.A[0]
                        # define only one interval, bacause functions share the interval of integration
                        sum_l, sum_r = (x_l_pp + x_l_p), (x_r_m + x_r_mm)
                        return A_l * A_r * (4 / 3 * diff(x_l_pp, _x_l, 3) - diff(x_l_pp, _x_l, 2) * (sum_l + sum_r) + sum_l * sum_r * diff(x_l_pp, _x_l, 1))
                    else: return 0
        else:
            odd, even = (self, bj) if self.f_odd else (bj, self)
            x_o_m, _x_o, x_o_p = odd.get_coords()
            _, x_e_m, _x_e, x_e_p, _ = even.get_coords()
            if _x_o == x_e_m or _x_o == x_e_p:
                A_o = odd.A
                A_e =  even.A[0] if _x_e < _x_o else even.A[1]
                # define only one interval, bacause functions share the interval of integration
                sum_o = (x_o_m + x_o_p)
                return A_o * A_e * (4 / 3 * diff(x_o_p, x_o_m, 3) - diff(x_o_p, x_o_m, 2) * 2 * sum_o + sum_o**2 * diff(x_o_p, x_o_m, 1))
            else: return 0


class BasisFactory():
    def get_basis_for_domain(disc_omega: np.ndarray, el_type: ElementType) -> List[Basis]:
        if el_type == ElementType.LIN:
            return [
                Hat((
                    disc_omega[i - 1],
                    disc_omega[i],
                    disc_omega[i + 1]
                )) for i in range(1, disc_omega.size - 1)]
        elif el_type == ElementType.QUAD:
            # differentiate odd and even cases
            return [
                Quadratic((
                    disc_omega[i - 2],
                    disc_omega[i - 1],
                    disc_omega[i], disc_omega[i + 1],
                    disc_omega[i + 2]
                )) if i % 2 == 0

                else Quadratic((
                    disc_omega[i - 1],
                    disc_omega[i],
                    disc_omega[i + 1]
                ))

                for i in range(1, disc_omega.size - 1)
            ]
        raise NotImplemented