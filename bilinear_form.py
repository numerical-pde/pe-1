from typing import List
from basis import Basis
from source import SourseFunction


class StiffnessMatrix:
    _f_constrcuted: bool = False
    """
    In Ritz-Galerkin sense := bilinear form.
    
    Params:
    basis: List[Basis] - list of basis functions for which the matrix is constructed
    """
    def __init__(self, basis: List[Basis]):
        self.basis = basis
    
    def get(self):
        if(self._f_constrcuted):
            return self.matrix
        else:
            self.matrix = [[bi | bj for bj in self.basis] for bi in self.basis]
            self._f_constrcuted = True
            return self.matrix
    
    def refresh(self):
        self._f_constrcuted = False
        return self


class RHS:
    _f_constrcuted: bool = False
    
    def __init__(self, source: SourseFunction, basis: List[Basis]):
        self.source = source
        self.basis = basis
    
    def get(self):
        if self._f_constrcuted:
            return self.vec
        else:
            self.vec = [self.source | b for b in self.basis]
            self._f_constrcuted = True
            return self.vec